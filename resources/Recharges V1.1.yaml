openapi: 3.0.0
info:
  title: Recharges
  version: 1.0.0
paths:
  /api/v1/recharges:
    post:
      tags:
        - Recharges
      description: This service will be used to make a recharge to a mobile line
      operationId: recharges
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/recharges'
            example:
              input:
                included:
                  newInstance:
                    attributes:
                      externalCode: '34234123000001'
                      msisdn: '573024599238'
                      externalRequestedAt: '2020-03-17T01:00:00Z'
                      sourceChannel: ESALES
                      characteristics:
                        - key: 'amount'
                          value: '2000'
                        - key: 'method'
                          value: 'cash'
                        - key: 'topUpCode'
                          value: '<<stipo_recarga>>'
                        - key: 'promotionalCode'
                          value: '<<scod_promocion>>'
                        - key: 'valididy'
                          value: '<<lper_validez>>'
                        - key: 'currency'
                          value: 'USD'
                        - key: 'userCode'
                          value: '<<userCode>>'
      parameters:
        - name: Content-Type
          in: header
          required: false
          description: application/json
          schema:
            type: string
            default: application/json
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/apiRechargesResponse200'
              example:
                data:
                  voucherInvocationCreate:
                    id: 34b1cca3-dd1a-47a3-b8ad-60a142ddff0e
                    lifecycleStatus: completed
                    function: voucher-invocations-create
                    requestedAt: '2020-03-05T16:34:00.287Z'
                    completedAt: '2020-03-05T16:34:01.481Z'
                    resource:
                      id: 5281e11f-b921-4340-aa38-36ce506e48ba
                    process:
                      characteristics:
                        entries:
                          - key: coreBalanceValue
                            value: '67800833'
                          - key: balanceDetailEndDateTime
                            value: '2036-12-31T20:00:00Z'
        '400':
          description: 'Bad Request: some parameter is wrong. Fix your code, client!'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response400'
        '500':
          description: >-
            Internal Server Error: if something wrong happened and you really
            have no idea why, you pass this back. Which ideally should be never.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response500'
        '503':
          description: >-
            Service Unavailable : we use this when a backend system is down,
            accompanied by an internal error code.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response503'
  '/api/v1/customerManagement/subscription/resources/{primaryId}/resourceType/{resourceType}':
    get:
      tags:
        - Customer Management - resources
      parameters:
        - name: primaryId
          in: path
          required: true
          description: >-
            The attributes resource-type and primary-id together identify the
            relevant entity in Resource Inventory if the resource-type is
            supported by that domain. Otherwise, this is a unique ID in an
            external system. Additionally, the resource-type determines the
            validation rules of the primary-id. In case of MSISDN the ID is an
            MSISDN, in case of a SIM the ID is an ICC number
          schema:
            type: string
        - name: resourceType
          in: path
          description: >-
            Exposes the type of the resource instance. This might be 1:1 with
            this.specification.spec-subtype or an aggregated value.
            logical-resource and physical-resources are used for general
            categorization with limited or no support in Resource Inventory
            domain e.g. for set-top boxes, cable tv identifiers, etc.
          required: true
          schema:
            type: string
            default: msisdn
            enum:
              - msisdn
              - sim
              - device
              - logical-resource
              - physical-resource
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: >-
                  #/components/schemas/apiCustomerManagementSubscriptionResourcesResponse200
              example:
                data:
                  resources:
                    edges:
                      - nodes:
                          primariyId: '59177330602'
                          lifecycleStatus: active
                          realizedProduct:
                            lifecycleStatus: active
        '204':
          description: >-
            Not Found: Only of the user explicitly requested a resource that
            doesn't exist. Don't use this for empty lists or in other creative
            ways as it confuses both the API consumer and creates noise in the
            monitoring systems.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/resourcesResponse204'
        '400':
          description: 'Bad Request: some parameter is wrong. Fix your code, client!'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response400'
        '500':
          description: >-
            Internal Server Error: if something wrong happened and you really
            have no idea why, you pass this back. Which ideally should be never.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response500'
        '503':
          description: >-
            Service Unavailable : we use this when a backend system is down,
            accompanied by an internal error code.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response503'
components:
  schemas:
    apiRechargesResponse200:
      $ref: '#/components/schemas/rechargesResponse'
    response404:
      type: object
      properties:
        data:
          type: object
          example: null
        errors:
          type: array
          items:
            properties:
              message:
                type: string
                example: BSS Server returned with error code 404 Not Found
              extensions:
                type: object
                properties:
                  status:
                    type: string
                    example: '404'
                  message:
                    type: string
                    example: Not Found
    response400:
      type: object
      properties:
        data:
          type: object
          example: null
        errors:
          type: array
          items:
            properties:
              message:
                type: string
                example: BSS Server returned with error code 400 Bad Request
              extensions:
                type: object
                properties:
                  status:
                    type: string
                    example: '400'
                  message:
                    type: string
                    example: Bad Request
    response500:
      type: object
      properties:
        data:
          type: object
          example: null
        errors:
          type: array
          items:
            properties:
              message:
                type: string
                example: BSS Server returned with error code 500 Internal Server Error
              extensions:
                type: object
                properties:
                  status:
                    type: string
                    example: '500'
                  message:
                    type: string
                    example: Internal Server Error
    response503:
      type: object
      properties:
        data:
          type: object
          example: null
        errors:
          type: array
          items:
            properties:
              message:
                type: string
                example: >-
                  BSS Server returned with error code 503 Service Unavailable
                  with answer {
                    "errors": [{
                      "status": "503 Service Unavailable",
                      "title": "The server is currently unavailable (because it is overloaded or down for maintenance)."
                    }]
                  }.
              extensions:
                type: object
                properties:
                  status:
                    type: string
                    example: '503'
                  message:
                    type: string
                    example: Service Unavailable
    response401:
      type: object
      properties:
        data:
          type: object
          example: null
        errors:
          type: array
          items:
            properties:
              message:
                type: string
                example: BSS Server returned with error code 400 Bad Request
              extensions:
                type: object
                properties:
                  status:
                    type: string
                    example: '400'
                  message:
                    type: string
                    example: Bad Request
    recharges:
      type: object
      required: 
        - input
      properties:
        input:
          type: object
          required: 
            - included
          properties:
            included:
              type: object
              required: 
                - newInstance
              properties:
                newInstance:
                  type: object
                  required: 
                    - attributes
                  properties:
                    attributes:
                      type: object
                      required: 
                        - externalCode
                        - msisdn
                        - externalRequestedAt
                        - sourceChannel
                        - characteristics
                      properties:
                        externalCode:
                          type: string
                          description: Internal unique identifier of the recharge operation request referenced
                        msisdn:
                          type: string
                        externalRequestedAt:
                          type: string
                        voucherCode:
                          type: string
                          description: Promotional identifier that can be used to grant the recharge operation or reduce cost
                        sourceChannel:
                          type: string
                          description: String providing the name of the related entity reported
                        characteristics:
                          type: array
                          required: 
                            - items
                          items:
                            type: object
                            required: 
                              - key
                              - value
                            properties:
                              key:
                                type: string 
                                description: '"reason": Request motivation, possible value: topup, topupReverse. "amount": Topup amount.  "method": Payment method (cash, credit card). "topUpCode" to specify the topup profile. "currency": Currency code (NIO, USD, etc). "topupId": To specify the currency to reverse where reason equals "topupReverse'
                                enum:
                                  - reason
                                  - amount
                                  - method
                                  - topUpCode
                                  - currency
                                  - topupId
                              value:
                                type: string
    rechargesResponse:
      type: object
      properties:
        data:
          type: object
          properties:
            voucherInvocationCreate:
              type: object
              properties:
                id:
                  type: string
                lifecycleStatus:
                  type: string
                function:
                  type: string
                requestedAt:
                  type: string
                completedAt:
                  type: string
                resource:
                  type: object
                  properties:
                    id:
                      type: string
                process:
                  type: object
                  properties:
                    characteristics:
                      type: object
                      properties:
                        entry:
                          type: array
                          items:
                            properties:
                              key:
                                type: string
                              value:
                                type: string
    apiCustomerManagementSubscriptionResourcesResponse200:
      type: object
      properties:
        data:
          type: object
          properties:
            resources:
              type: object
              properties:
                edges:
                  type: array
                  items:
                    type: object
                    properties:
                      node:
                        type: object
                        properties:
                          primaryId:
                            type: string
                          lifecycleStatus:
                            type: string
                          realizedProduct:
                            type: object
                            properties:
                              lifecycleStatus:
                                type: string
    resourcesResponse204:
      type: object
      properties:
        data:
          type: object
          properties:
            resources:
              type: object
              properties:
                edges:
                  type: array
                  items:
                    properties:
                      node:
                        type: object
