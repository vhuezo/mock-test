FROM node:10
WORKDIR /mock
MAINTAINER  vhuezo
ENV PORT="2020"
ADD . .
RUN npm install 
CMD npm start --host 0.0.0.0 --port 2020
EXPOSE 2020
